{ pkgs, ... }:
{

  time.timeZone = "Europe/Paris";

  system.activationScripts.extraActivation.text = ''
    if ! /usr/bin/arch -x86_64 /usr/bin/true 2>/dev/null; then
        softwareupdate --install-rosetta --agree-to-license
    fi
    # Disable the sound effects on boot
    nvram SystemAudioVolume=" "
    # Show the /Volumes folder
    chflags nohidden /Volumes
    # Show language menu in the top right corner of the boot screen
    defaults write /Library/Preferences/com.apple.loginwindow showInputMenu -bool true
  '';
  system.activationScripts.postUserActivation.text = ''
    # Set fish shell as default
    FISH_SHELL="/run/current-system/sw/bin/fish"
    # Get the current shell for the user running the script
    CURRENT_SHELL=$(dscl . -read "$HOME" UserShell | cut -d ' ' -f 2)
    if grep -qx "$FISH_SHELL" /etc/shells && [ "$CURRENT_SHELL" != "$FISH_SHELL" ]; then
        chsh -s "$FISH_SHELL"
    fi
    # ln asdf <> mise
    if [ -e ~/.local/share/mise ] && [ ! -e ~/.asdf ]; then
      ln -s ~/.local/share/mise ~/.asdf
    fi
    # ln docker config
    if [ -e ~/.config/containers/auth.json ] && [ ! -e ~/.docker/config.json ]; then
      ln -s ~/.config/containers/auth.json ~/.docker/config.json
    fi
    # Open home by default
    defaults write com.apple.finder NewWindowTarget -string "PfLo"
    defaults write com.apple.finder NewWindowTargetPath -string "file://$HOME/"
    # Preview pane by default
    defaults write com.apple.finder ShowPreviewPane -bool true
    # Show drives by default on desktop
    defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
    defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
    defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
    defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true
    # Disable window animations and Get Info animations
    defaults write com.apple.finder DisableAllAnimations -bool true
    # Drop trash files after 30 days
    defaults write com.apple.finder "FXRemoveOldTrashItems" -bool true
    # Show the ~/Library folder
    chflags nohidden ~/Library
    # Shows battery percentage
    defaults write com.apple.menuextra.battery ShowPercent YES
    # Avoid creating .DS_Store files on network or USB volumes
    defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
    defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
    # Disable autocorrect & autocomplete in textedit and cie…
    defaults write -g NSAutomaticSpellingCorrectionEnabled -bool false
    defaults write -g NSAutomaticTextCompletionEnabled -bool false
    # TextEdit related, I expect to remove automagical features, because I use textEdit as a simple text editor.
    defaults write com.apple.TextEdit SmartCopyPaste -bool false
    defaults write com.apple.TextEdit RichText -bool false
    defaults write com.apple.TextEdit SmartQuotes -bool false
    defaults write com.apple.TextEdit SmartDashes -bool false
    # Enable airdrop over wired ethernet
    defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1
    # Restart services & reloads to prevent login/logout
    /System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u
    killall Finder
    killall SystemUIServer

  '';
  system.defaults = {
    # Appareance ; switch dark/light automatically
    NSGlobalDomain.AppleInterfaceStyleSwitchesAutomatically = true;
    # Show file extensions
    finder.AppleShowAllExtensions = true;
    NSGlobalDomain.AppleShowAllExtensions = true;

    # Show all files
    finder.AppleShowAllFiles = true;
    NSGlobalDomain.AppleShowAllFiles = true;

    finder.FXPreferredViewStyle = "clmv"; # Default column view
    finder.ShowPathbar = true; # Show path bar
    finder.ShowStatusBar = true; # Show status bar
    finder.FXDefaultSearchScope = "SCcf"; # Search in current folder by default
    finder._FXShowPosixPathInTitle = true; # Full POSIX filepath in the window title

    NSGlobalDomain.NSWindowShouldDragOnGesture = true; # moving window by holding anywhere

    NSGlobalDomain.NSAutomaticCapitalizationEnabled = false;
    NSGlobalDomain.NSAutomaticDashSubstitutionEnabled = false;
    NSGlobalDomain.NSAutomaticPeriodSubstitutionEnabled = false;
    NSGlobalDomain.NSAutomaticQuoteSubstitutionEnabled = false;
    NSGlobalDomain.NSAutomaticSpellingCorrectionEnabled = false;

    dock.show-recents = false;
    dock.persistent-apps = [
      "/Applications/Firefox Developer Edition.app"
      "/Applications/Ghostty.app"
    ];
    dock.magnification = true;
    dock.tilesize = 16;
    dock.showhidden = true;
    dock.autohide = false;
    dock.mru-spaces = false;
    dock.orientation = "bottom";

    screencapture.disable-shadow = true;
    screencapture.location = "~/Desktop/screenshots";

    ".GlobalPreferences"."com.apple.mouse.scaling" = 10.0;
    magicmouse.MouseButtonMode = "TwoButton";
  };
}
