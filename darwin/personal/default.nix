{ pkgs, ... }:
{
  imports = [ ./homebrew.nix ];
  system.defaults.dock.persistent-apps = [
    "/Applications/Microsoft Outlook.app"
    "/Applications/Freeform.app"
  ];
}
