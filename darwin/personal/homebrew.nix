{ ... }:
{
  homebrew.casks = [
    # Development
    "adobe-creative-cloud" # To use same tools than designers (I've licences)
    "google-chrome"

    # Cloud
    "nextcloud"

    # Security
    "little-snitch" # Firewall (I've a licence)
    "1password" # Password management (I've two licences : 1 pro, 1 personal)
    "qflipper" # For flipper zero

    # Entertainment
    "steam"
    "minecraft"
    "transmission-remote-gui" # I use it to remote connect to my seedbox
    "calibre" # To manage ebooks
    "adobe-digital-editions" # To download some ebooks
    "snapmaker-luban"
  ];
  masApps = {
    "iMazing HEIC Converter" = 1292198261; # Used to convert HEIC to JPG
    Excel = 462058435;
    Word = 462054704;
    PowerPoint = 462062816;
  };
}
