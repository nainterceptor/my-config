{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Ensure podman is installed
  environment.systemPackages = with pkgs; [
    podman
    podman-compose
  ];

  # Enable system-wide Podman
  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
      # Create a docker alias linking to podman
      dockerSocket.enable = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  # User configuration
  users.users.panda.extraGroups = [ "podman" ];
}
