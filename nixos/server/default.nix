{
  config,
  lib,
  pkgs,
  ...
}:

{
  imports = [
    ./podman.nix
    ./services.nix
    ./services/nginx.nix
    ./fail2ban.nix
    ./security.nix
    ./services/transmission.nix
    ./services/directus.nix
    ./services/personal-website.nix
  ];

  programs.fish.enable = true;
  users.defaultUserShell = pkgs.fish;

  # Basic server configuration
  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [
        80
        443
        22 # SSH
      ];
      logRefusedConnections = true;
      checkReversePath = "strict";
    };
  };
}
