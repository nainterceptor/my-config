{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Configure fail2ban
  services.fail2ban = {
    enable = true;
    # Use systemd journal as log source
    bantime = "24h";
    maxretry = 3;

    jails = {
      # Protect Nginx from brute force attacks
      nginx-http-auth = ''
        enabled = true
        port = http,https
        filter = nginx-http-auth
        logpath = /var/log/nginx/error.log
        maxretry = 3
        findtime = 1h
        bantime = 24h
      '';
    };
  };
}
