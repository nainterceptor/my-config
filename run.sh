#!/bin/bash

OS=$(uname)

# Function to install Nix
install_nix() {
  if ! command -v nix >/dev/null 2>&1; then
    echo "Installing Nix…"
    EXPECTED_SHASUM=28e17f77ba8938a9b2beb6ba026502893989972f
    curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix -o /tmp/nix-install.sh
    chmod +x /tmp/nix-install.sh
    DOWNLOAD_SHASUM=$(shasum /tmp/nix-install.sh | awk '{print $1}')

    if [ "$EXPECTED_SHASUM" != "$DOWNLOAD_SHASUM" ]; then
      echo "Error: expected Nix installation script shasum is incorrect"
      echo "Setup cancelled"
      exit 1
    else
      sh /tmp/nix-install.sh install --no-confirm --determinate
      # shellcheck disable=SC1091
      . /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
      echo "Nix installed"
    fi
  else
    echo "Nix already installed"
  fi
}

nix_mutation() {
    export NIX_CHANNEL=nixos-24.11
    if ! command -v nix >/dev/null 2>&1; then
    echo "Installing Nix…"
    EXPECTED_SHASUM=ca1f327d734a1002e44f6772873f0395d159fbf7
    curl --proto '=https' --tlsv1.2 -sSf -L https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect -o /tmp/nix-install.sh
    chmod +x /tmp/nix-install.sh
    DOWNLOAD_SHASUM=$(shasum /tmp/nix-install.sh | awk '{print $1}')

    if [ "$EXPECTED_SHASUM" != "$DOWNLOAD_SHASUM" ]; then
      echo "Error: expected Nix installation script shasum is incorrect"
      echo "Setup cancelled"
      exit 1
    else
      bash /tmp/nix-install.sh
      echo "Nix installed"
    fi
  else
    echo "Nix already installed"
  fi
}

# Function to install Homebrew (only for macOS)
install_brew() {
  if [ "$OS" = "Darwin" ]; then
    if ! command -v brew >/dev/null 2>&1; then
      echo "Installing Homebrew…"
      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    else
      echo "Homebrew already installed"
    fi
  fi
}

# Function to generate NixOS configuration content
generate_nixos_config() {
  local hostname=$1
  local username=$2
  local ssh_keys=$3

  cat << EOF
{lib, ...}:
{
  networking.hostName = lib.mkForce "${hostname}";
  security.sudo.wheelNeedsPassword = false;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  users.users."${username}" = {
    isNormalUser = true;
    home = "/home/panda";
    extraGroups = [ "wheel" "networkmanager" ];
    openssh.authorizedKeys.keys = [
${ssh_keys}
    ];
  };
}
EOF
}

prepare_debian() {
  [[ "$(whoami)" == "root" ]] || { echo "ERROR: Must run as root"; return 1; }
  apt install curl vim sudo

  hostname=${NIXOS_HOSTNAME:-$(hostname)}
  username=${NIXOS_USERNAME:-panda}

  # Copy SSH keys from user to root if they exist
  if [ -f "/home/${username}/.ssh/authorized_keys" ]; then
    mkdir -p /root/.ssh
    chmod 700 /root/.ssh
    cat "/home/${username}/.ssh/authorized_keys" >> /root/.ssh/authorized_keys
    sort -u /root/.ssh/authorized_keys -o /root/.ssh/authorized_keys
    chmod 600 /root/.ssh/authorized_keys
  fi

  echo "Creating Nix configuration file..."
  echo "Using hostname: $hostname"
  echo "Using username: $username"

  # Get current user's SSH authorized keys and format them for Nix
  SSH_KEYS=""
  if [ -f ~/.ssh/authorized_keys ]; then
    while IFS= read -r key; do
      if [ -n "$key" ]; then
        SSH_KEYS="${SSH_KEYS} ''${key}''"
      fi
    done < ~/.ssh/authorized_keys
  fi

  # Ensure /etc/nixos directory exists
  mkdir -p /etc/nixos

  # Generate and write configuration
  generate_nixos_config "$hostname" "$username" "$SSH_KEYS" > /etc/nixos/default.nix
  echo "Nix configuration file created at /etc/nixos/default.nix"
}

prepare_nixos() {
  echo "Installing Git using Nix…"
  nix-env -i git

  # Check if configuration.nix exists and contains a reference to default.nix
  if [ -f "/etc/nixos/configuration.nix" ]; then
    if grep -q "default.nix" "/etc/nixos/configuration.nix"; then
      # Ensure default.nix exists
      if [ ! -f "/etc/nixos/default.nix" ]; then
        echo "Recreating missing default.nix..."
        generate_nixos_config "$(hostname)" "$(whoami)" "" > /etc/nixos/default.nix
      fi
    fi
  fi
}

# Function to configure computer name
configure_hostname() {
  local current_hostname
  current_hostname=$(hostname)
  read -r -p "Enter computer name [$current_hostname]: " new_hostname
  new_hostname=${new_hostname:-$current_hostname}

  if [ "$new_hostname" != "$current_hostname" ]; then
    echo "Changing hostname to $new_hostname..."
    if [ "$OS" = "Darwin" ]; then
      sudo scutil --set ComputerName "$new_hostname"
      sudo scutil --set HostName "$new_hostname"
      sudo scutil --set LocalHostName "$new_hostname"
    fi
  fi
}

# Configure the shared config
setup_config() {
    GIT_BRANCH=nix
    mkdir -p $CONFIG_BASEPATH
    if [ ! -d "$CONFIG_PATH" ]; then
      # To being able to first checkout without having configured keys in gitlab CI
      git -C $CONFIG_BASEPATH clone https://gitlab.com/pand.app/config.git $CONFIG_FOLDER -b $GIT_BRANCH
      # But we're configuring as ssh to being able to push later
      git -C $CONFIG_PATH remote set-url origin git@gitlab.com:pand.app/config.git
    else
      git -C $CONFIG_PATH pull
    fi
}

# Function to handle Darwin-specific setup
setup_darwin() {
  if ! command -v darwin-rebuild >/dev/null 2>&1; then
    echo "Installing darwin-rebuild…"
    nix run nix-darwin -- switch --flake $CONFIG_PATH
  else
    nix flake update
    darwin-rebuild switch --flake $CONFIG_PATH
    echo "darwin-rebuild already installed"
  fi
}

# Function to handle NixOS-specific setup
setup_nixos() {
  U=$(whoami)
  if [ -f /etc/nixos/hardware-configuration.nix ]; then
    echo "Copying existing hardware configuration..."
    sudo cp /etc/nixos/hardware-configuration.nix $CONFIG_PATH/
    sudo chown "$U":users "$CONFIG_PATH/hardware-configuration.nix"
  else
    echo "Generating hardware configuration..."
    nixos-generate-config --dir $CONFIG_PATH
  fi

  if [ -f /etc/nixos/default.nix ]; then
    echo "Copying existing default.nix..."
    sudo cp /etc/nixos/default.nix $CONFIG_PATH/
    sudo chown "$U":users "$CONFIG_PATH/default.nix"
  else
    echo "Generating default.nix..."
    generate_nixos_config "$(hostname)" "$U" "" > $CONFIG_PATH/default.nix
  fi
  sudo chown "$U":users "$CONFIG_PATH/default.nix"

  sudo git -C $CONFIG_PATH add hardware-configuration.nix default.nix
  echo "Running nixos-rebuild…"
  sudo nixos-rebuild switch --accept-flake-config --flake $CONFIG_PATH
}

export CONFIG_FOLDER=pandapp
export CONFIG_BASEPATH=~/.config
export CONFIG_PATH=$CONFIG_BASEPATH/$CONFIG_FOLDER
if [ "$OS" = "Darwin" ]; then
  install_nix
  install_brew
  configure_hostname
  setup_config
  setup_darwin
elif [ -f /etc/NIXOS ]; then
  prepare_nixos
  setup_config
  setup_nixos
elif [ -f /etc/debian_version ]; then
  prepare_debian
  export NIXOS_IMPORT=/etc/nixos/default.nix
  nix_mutation
  echo "Should be already reboot ; please rerun script"
else
  echo "Unsupported operating system"
  exit 1
fi
