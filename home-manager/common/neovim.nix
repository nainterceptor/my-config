{ config, pkgs, ... }:
{
  imports = [
    # General Configuration
    ./neovim/theme.nix
    ./neovim/settings.nix
    ./neovim/keymaps.nix
    ./neovim/auto_cmds.nix
    ./neovim/file_types.nix

    ./neovim/plugins/cmp/autopairs.nix
    ./neovim/plugins/cmp/cmp-copilot.nix
    ./neovim/plugins/cmp/cmp.nix
    ./neovim/plugins/cmp/lspkind.nix
    ./neovim/plugins/cmp/schemastore.nix

    ./neovim/plugins/editor/copilot.nix
    ./neovim/plugins/editor/illuminate.nix
    ./neovim/plugins/editor/luasnip.nix
    ./neovim/plugins/editor/navic.nix
    ./neovim/plugins/editor/neo-tree.nix
    ./neovim/plugins/editor/todo-comments.nix
    ./neovim/plugins/editor/treesitter.nix

    ./neovim/plugins/git/gitsigns.nix
    ./neovim/plugins/git/lazygit.nix

    ./neovim/plugins/lsp/conform.nix
    ./neovim/plugins/lsp/fidget.nix
    ./neovim/plugins/lsp/lsp.nix

    ./neovim/plugins/ui/bufferline.nix
    ./neovim/plugins/ui/lualine.nix
    ./neovim/plugins/ui/startup.nix

    ./neovim/plugins/utils/markdown-preview.nix
    ./neovim/plugins/utils/mini.nix
    ./neovim/plugins/utils/telescope.nix
    ./neovim/plugins/utils/toggleterm.nix
  ];

  home.sessionVariables.EDITOR = "nvim";
  programs.nixvim = {
    enable = true;
    viAlias = true;
    vimAlias = true;

    globals.mapleader = ",";

    plugins.emmet.enable = true;
    plugins.ts-autotag.enable = true;
    plugins.lz-n.enable = true;
    plugins.indent-blankline.enable = true;
    # utils
    plugins.web-devicons.enable = true;
    plugins.which-key.enable = true;
  };
}
